//
//  PullRequestModelTests.swift
//  Desafio-iOS
//
//  Created by Rafael Menezes on 24/12/16.
//  Copyright © 2016 Rafael Menezes. All rights reserved.
//

import XCTest
@testable import Desafio_iOS
import ObjectMapper

class PullRequestModelTests: XCTestCase {
    let mapData: [String: AnyObject] = [
        "title":"Teste 1",
        "html_url":"http://localhost.com/teste",
        "created_at":"2016-03-31T19:39:54Z",
        "user":[
            "login":"teste",
            "avatar_url":"http://localhost.com/teste",
        ],
        "body": "Test to body"
    ]
    
    func testIsIquals() {
        let pullRequest = Mapper<PullRequestModel>().map(JSON: mapData)
        XCTAssertEqual("teste", pullRequest!.authorName, "The authorName is iquals")
        XCTAssertEqual("http://localhost.com/teste", pullRequest!.avatarURL, "The avatarURL is iquals")
        XCTAssertEqual("2016-03-31T19:39:54Z", pullRequest!.date, "The date is iquals")
        XCTAssertEqual("Teste 1", pullRequest!.title, "The title is iquals")
        XCTAssertEqual("http://localhost.com/teste", pullRequest!.url, "The url is iquals")
        XCTAssertEqual("Test to body", pullRequest!.body, "The body is iquals")
    }
    
    func testNotIquals() {
        let pullRequest = Mapper<PullRequestModel>().map(JSON: mapData)
        XCTAssertNotEqual("teste 1", pullRequest!.authorName, "The authorName isn't iquals")
        XCTAssertNotEqual("http://localhost.com/teste1", pullRequest!.avatarURL, "The avatarURL isn't iquals")
        XCTAssertNotEqual("2016-03-30T19:39:54Z", pullRequest!.date, "The date isn't iquals")
        XCTAssertNotEqual("Teste 2", pullRequest!.title, "The title isn't iquals")
        XCTAssertNotEqual("http://localhost.com/teste1", pullRequest!.url, "The url isn't iquals")
        XCTAssertNotEqual("Test to bud", pullRequest!.body, "The body isn't iquals")
    }
    
}
