//
//  RepositoryModelTests.swift
//  Desafio-iOS
//
//  Created by Rafael Menezes on 24/12/16.
//  Copyright © 2016 Rafael Menezes. All rights reserved.
//

import XCTest
@testable import Desafio_iOS
import ObjectMapper

class RepositoryModelTests: XCTestCase {
    
    let mapData: [String: AnyObject] = [
        "name": "elasticsearch",
        "owner":[
            "login": "elastic",
            "avatar_url": "https://avatars.githubusercontent.com/u/6764390?v=3"
        ],
        "description": "Open Source, Distributed, RESTful Search Engine",
        "stargazers_count": 15779,
        "forks": 5274,
    ]
    
    func testIsIquals() {
        
        let repository = Mapper<RepositoryModel>().map(JSON: mapData)
        XCTAssertEqual("elasticsearch", repository!.repository_name, "The name is iquals")
        XCTAssertEqual("elastic", repository!.login, "The login is iquals")
        XCTAssertEqual("https://avatars.githubusercontent.com/u/6764390?v=3", repository!.avatar_url, "The avatar_url is iquals")
        XCTAssertEqual("Open Source, Distributed, RESTful Search Engine", repository!.repoDescription, "The repoDescription is iquals")
        XCTAssertEqual(15779, repository!.stars, "The Starts is iquals")
        XCTAssertEqual(5274, repository!.forks, "The forks is iquals")
        
    }
    
    func testNotIquals() {
        
        let repository = Mapper<RepositoryModel>().map(JSON: mapData)
        XCTAssertNotEqual("elasticsearch1", repository!.repository_name, "The name isn't iquals")
        XCTAssertNotEqual("elastic1", repository!.login, "The login isn't iquals")
        XCTAssertNotEqual("https://avatars.githubusercontent.com/u/6764390?v=31", repository!.avatar_url, "The avatar_url isn't iquals1")
        XCTAssertNotEqual("Open Source, Distributed, RESTful Search Engine1", repository!.repoDescription, "The repoDescription isn't iquals")
        XCTAssertNotEqual(157791, repository!.stars, "The Starts isn't iquals")
        XCTAssertNotEqual(52741, repository!.forks, "The forks isn't iquals")
    }
    
}
