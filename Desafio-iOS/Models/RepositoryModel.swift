//
//  RepositoryModel.swift
//  Desafio_iOS
//
//  Created by Rafael Menezes on 24/12/16.
//  Copyright © 2016 Rafael Menezes. All rights reserved.
//

import UIKit
import ObjectMapper

class RepositoryModel: Mappable {
    
    var repository_name:String?
    var owner: OwnerModel?
    var repoDescription:String?
    var stars:Int?
    var forks:Int?

    required init?(map: Map) {
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        repository_name <- map["name"]
        owner <- map["owner"]
        repoDescription <- map["description"]
        forks <- map["forks"]
        stars <- map["stargazers_count"]
    }
}
