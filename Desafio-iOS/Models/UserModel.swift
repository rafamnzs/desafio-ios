//
//  User.swift
//  Desafio-iOS
//
//  Created by RR Lab on 24/12/16.
//  Copyright © 2016 RRLab. All rights reserved.
//

import UIKit
import ObjectMapper

class UserModel: Mappable {
    
    var authorName:String?
    var avatarURL:String?
    
    required init?(map: Map) {
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        authorName <- map["login"]
        avatarURL <- map["avatar_url"]
    }
}
