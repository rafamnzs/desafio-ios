//
//  PullRquestModel.swift
//  Desafio_iOS
//
//  Created by Rafael Menezes on 24/12/16.
//  Copyright © 2016 Rafael Menezes. All rights reserved.
//

import UIKit
import ObjectMapper

class PullRequestModel: Mappable {
    
    var user: UserModel?
    var title:String?
    var body:String?
    var url:String?
    var date:String?
    var state: String?
    
    required init?(map: Map) {
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        user <- map["user"]
        title <- map["title"]
        body <- map["body"]
        url <- map["html_url"]
        date <- map["created_at"]
        state <- map["state"]
    }
}
