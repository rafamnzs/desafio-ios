//
//  RepositoryTableViewCell.swift
//  Desafio_iOS
//
//  Created by Rafael Menezes on 24/12/16.
//  Copyright © 2016 RRLab. All rights reserved.
//

import UIKit
import Kingfisher
import Font_Awesome_Swift

class RepositoryTableViewCell: UITableViewCell {
    
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var forkLbl: UILabel!
    @IBOutlet weak var starLbl: UILabel!
    @IBOutlet weak var repositoryNameLbl: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    
    var repositoryModel:RepositoryModel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.userImage.layer.cornerRadius = self.userImage.frame.width / 2
        self.userImage.clipsToBounds = true;
        self.repositoryNameLbl.textColor = UIColor().customBlue()
        self.userNameLbl.textColor = UIColor().customGrey()
        self.forkLbl.textColor = UIColor().customYellow()
        self.starLbl.textColor = UIColor().customYellow()
        
        self.userNameLbl.adjustsFontSizeToFitWidth = true;
        self.userNameLbl.minimumScaleFactor = 0.5
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func configCell(model: RepositoryModel) {
        if let avatar_url = model.owner?.avatarURL{
            let url = URL(string: avatar_url)
            self.userImage.kf.setImage(with: url!)
        }
        
        if let authorName = model.owner?.authorName{
            self.userNameLbl.text = authorName
        }
        
        if let repositoryName = model.repository_name{
            self.repositoryNameLbl.text = repositoryName
        }
        
        if let repositoryDescription = model.repoDescription{
            self.descriptionLbl.text = repositoryDescription
        }
        
        if let forksValue = model.forks{
            if forksValue <= 0  {
                self.forkLbl.setFAText(prefixText: "", icon: FAType.FACodeFork, postfixText: " \(0)", size: 17)
            }
            else{
                self.forkLbl.setFAText(prefixText: "", icon: FAType.FACodeFork, postfixText: " \(forksValue)", size: 17)
            }
        }
        
        if let starValue = model.stars{
            if starValue <= 0 {
                self.starLbl.setFAText(prefixText: "", icon: FAType.FAStar, postfixText: " \(0)", size: 17)
            }
            else{
                self.starLbl.setFAText(prefixText: "", icon: FAType.FAStar, postfixText: " \(starValue)", size: 17)
            }
        }
    }

}
