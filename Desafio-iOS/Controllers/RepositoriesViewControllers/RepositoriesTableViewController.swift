//
//  RepositoriesTableViewController.swift
//  Desafio_iOS
//
//  Created by Rafael Menezes on 24/12/16.
//  Copyright © 2016 RRLab. All rights reserved.
//

import UIKit

let estimatedRowHeightRespositoryCell:CGFloat = 126.0
let estimatedRowHeightPaginationCell:CGFloat = 49.0

class RepositoriesTableViewController: UITableViewController {

    var repositories = [RepositoryModel]()
    var limitPage:Int!
    var page:Int!
    let requestRepositories = Connection()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "GitHub JavaPop"
        self.tableView.estimatedRowHeight = estimatedRowHeightRespositoryCell
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.separatorColor = UIColor.clear
        self.page = 1
        self.seachdata(self.page)
    }
    
    //Get data from API with pagination.
    fileprivate func seachdata(_ page:Int){
        self.requestRepositories.getRepositories(page, completion: { (repositories) in
                self.repositories.append(contentsOf: repositories)
                self.tableView.separatorColor = UIColor.gray
                self.tableView.reloadData()
            }){ (limitePage) in
                self.limitPage = limitePage
            }
    }
    
    // MARK: - Table view delegate
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == self.repositories.count - 1 {
            return estimatedRowHeightPaginationCell
        }
        return UITableViewAutomaticDimension
    }
    
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.repositories.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == self.repositories.count - 1{
            if self.page <= self.limitPage{
                let cell = tableView.dequeueReusableCell(withIdentifier: "loadingCell", for: indexPath)
                let indicator:UIActivityIndicatorView = cell.viewWithTag(201) as! UIActivityIndicatorView
                indicator.startAnimating()
                self.page = self.page + 1
                self.seachdata(self.page)
                return cell
            }
        }

        let cell = tableView.dequeueReusableCell(withIdentifier: "repositoryCell", for: indexPath) as! RepositoryTableViewCell
        cell.configCell(model: repositories[indexPath.row])
    
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        var pullRequestView = PullRequestsViewController()
            pullRequestView = storyboard.instantiateViewController(withIdentifier: "PullRequestsView") as! PullRequestsViewController
            pullRequestView.repositoryModel = self.repositories[indexPath.row]
        self.navigationController!.pushViewController(pullRequestView, animated: true)
    }
}


