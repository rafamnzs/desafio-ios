//
//  PullRequestsViewController.swift
//  Desafio_iOS
//
//  Created by Rafael Menezes on 24/12/16.
//  Copyright © 2016 Rafael Menezes. All rights reserved.
//

import UIKit

let estimatedRowHeightPullRequestCell: CGFloat = 135

class PullRequestsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate{

    @IBOutlet weak var pullRequestTableView: UITableView!
    @IBOutlet weak var countLabel: UILabel!
    
    var userName:String!
    var repositoryName:String!
    var pullRequests:[PullRequestModel] = []
    var repositoryModel:RepositoryModel!
    var requestPullRequest = Connection()
    var open = [AnyObject]()
    var closed = [AnyObject]()
    
    override func viewWillAppear(_ animated: Bool) {
        self.pullRequestTableView.separatorColor = UIColor.clear
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.pullRequestTableView.delegate = self
        self.pullRequestTableView.dataSource = self
        self.countLabel.isHidden = true
        
        if let valueName = repositoryModel.owner?.login{
            self.userName = valueName
        }
        if let valueRepositoryName = repositoryModel.repository_name{
            self.repositoryName = valueRepositoryName
        }
        self.title = self.repositoryName
        
        self.pullRequestTableView.estimatedRowHeight = estimatedRowHeightPullRequestCell
        self.pullRequestTableView.rowHeight = UITableViewAutomaticDimension
        
        //Request Method
        self.requestPullRequest.getPullRequests(self.userName, repositoryName: self.repositoryName) { (pullRequests) in
            self.pullRequests = pullRequests
            if self.pullRequests.count != 0{
                self.countLabel.isHidden = false
                self.countLabel.attributedText = self.ColorText(UIColor().customYellow(), closedColor: UIColor.black)
            }
            self.pullRequestTableView.reloadData()
            self.pullRequestTableView.separatorColor = UIColor.gray
        }
    }
    
    //The code to change color CountLabel.
    func ColorText(_ openColor: UIColor, closedColor:UIColor) -> NSMutableAttributedString {
        let open = pullRequests.filter({$0.state == "open"})
        let closed = pullRequests.filter({$0.state == "closed"})
        let firstAttributes = [NSForegroundColorAttributeName: openColor]
        let secondAttributes = [NSForegroundColorAttributeName: closedColor]
        let textOpened = NSMutableAttributedString(string: "\(open.count) opened", attributes: firstAttributes)
        let textClosed = NSMutableAttributedString(string: " / \(closed.count) closed", attributes: secondAttributes)
        let result = NSMutableAttributedString()
            result.append(textOpened)
            result.append(textClosed)
        return result
    }

    // MARK: - Table view data source
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.pullRequests.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PullRequestCell", for: indexPath) as! PullRequestTableViewCell
        cell.configCell(model: pullRequests[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let pullRequest = pullRequests[indexPath.row]
        if let authorName = pullRequest.url{
            if let url = URL(string: authorName) {
                UIApplication.shared.openURL(url)
            }
        }
    }

}
