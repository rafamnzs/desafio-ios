//
//  PullRequestTableViewCell.swift
//  Desafio_iOS
//
//  Created by Rafael Menezes on 24/12/16.
//  Copyright © 2016 Rafael Menezes. All rights reserved.
//

import UIKit
import SwiftDate
import Kingfisher

class PullRequestTableViewCell: UITableViewCell {

    @IBOutlet weak var ownerPullRequestImageProfile: UIImageView!
    @IBOutlet weak var ownerPullRequestName: UILabel!
    @IBOutlet weak var pullRequestDate: UILabel!
    @IBOutlet weak var pullRequestTitle: UILabel!
    @IBOutlet weak var pullRequestBody: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.pullRequestTitle.textColor = UIColor().customBlue()
        self.ownerPullRequestName.textColor = UIColor().customGrey()
        self.ownerPullRequestImageProfile.layer.cornerRadius = self.ownerPullRequestImageProfile.frame.width / 2
        self.ownerPullRequestImageProfile.clipsToBounds = true;
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configCell(model: PullRequestModel) {
        if let avatar_url = model.user?.avatarURL{
            let url = URL(string: avatar_url)
            self.ownerPullRequestImageProfile.kf.setImage(with: url!)
        }
        
        if let authorName = model.user?.authorName{
            self.ownerPullRequestName.text = authorName
        }
        
        if let datePullRequest = model.date {
            self.pullRequestDate.text = datePullRequest.substring(to: datePullRequest.index(datePullRequest.startIndex, offsetBy: 10))
        }
        
        if let pullRequestTitle = model.title{
            self.pullRequestTitle.text = pullRequestTitle
        }
        
        if let pullRequestBody = model.body{
            self.pullRequestBody.text = pullRequestBody
        }
    }

}
