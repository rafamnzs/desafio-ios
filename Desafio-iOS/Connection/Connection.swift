//
//  Connection.swift
//  Desafio_iOS
//
//  Created by Rafael Menezes on 24/12/16.
//  Copyright © 2016 Rafael Menezes. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
import AlamofireObjectMapper

class Connection {
    
    func getRepositories(_ page:Int, completion:@escaping ([RepositoryModel]) -> Void, limitPage: @escaping (_ page: Int) -> Void) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        Alamofire.request(Routes.repositoriesURL(page))
        .validate(statusCode: 200..<300)
        .responseJSON { response in
            if let JSON = response.result.value {
                let totalPages:Int = (JSON as AnyObject).object(forKey:"total_count") as! Int
                limitPage(totalPages)

            }
        }
        .responseArray(keyPath:"items") { (response: DataResponse<[RepositoryModel]>) in
           let result = response.result.value
           var repositories = [RepositoryModel]()
           if let repos = result{
                repositories.append(contentsOf: repos)
           }
           completion(repositories)
           UIApplication.shared.isNetworkActivityIndicatorVisible = false
        }

    }
    
    func getPullRequests(_ userName:String, repositoryName:String, completion:@escaping ([PullRequestModel]) -> Void) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        Alamofire.request(Routes.pullRequestsURL(userName, repositoryName: repositoryName))
        .validate(statusCode: 200..<300)
        .responseArray { (response: DataResponse<[PullRequestModel]>) in
            let result = response.result.value
            if let pulls = result{
                completion(pulls)
            }
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
        }
   }
}
