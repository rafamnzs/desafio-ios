//
//  Route.swift
//  Desafio_iOS
//
//  Created by Rafael Menezes on 24/12/16.
//  Copyright © 2016 Rafael Menezes. All rights reserved.
//

import UIKit

enum baseURL:String{
    case PROD = "https://api.github.com/"
}

enum complementURL:String {
    case repostories = "search/repositories?q=language:Java&sort=stars&page={:page}"
    case pullrequests = "repos/{:username}/{:repostoryname}/pulls?state=all"
}

class Routes {
    class func repositoriesURL(_ page:Int) -> String {
        let URL = baseURL.PROD.rawValue + complementURL.repostories.rawValue
            .replacingOccurrences(of: "{:page}", with:"\(page)")
        return URL
    }
    
    class func pullRequestsURL(_ userName:String, repositoryName:String) -> String {
        let URL = baseURL.PROD.rawValue + complementURL.pullrequests.rawValue
            .replacingOccurrences(of: "{:username}", with: userName)
            .replacingOccurrences(of: "{:repostoryname}", with: repositoryName)
        return URL
    }
}
