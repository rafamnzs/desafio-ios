//
//  UIColor+Custom.swift
//  Desafio_iOS
//
//  Created by Rafael Menezes on 24/12/16.
//  Copyright © 2016 Rafael Menezes. All rights reserved.
//

import UIKit

extension UIColor {
    func customYellow() -> UIColor{
        return UIColor(red: 255.0/255.0, green: 201.0/255.0, blue: 33.0/255.0, alpha: 1.0)
    }
    
    func customBlue() -> UIColor{
        return UIColor(red: 34.0/255.0, green: 148.0/255.0, blue: 255.0/255.0, alpha: 1.0)
    }
    
    func customGrey() -> UIColor{
        return UIColor(red: 136.0/255.0, green: 163.0/255.0, blue: 196.0/255.0, alpha: 1.0)
    }
}
