//
//  String+Custom.swift
//  Desafio_iOS
//
//  Created by Rafael Menezes on 24/12/16.
//  Copyright © 2016 Rafael Menezes. All rights reserved.
//

import UIKit

extension String {
    func ambiguousDateFormat() -> String {
        return "dd/MM/yyyy"
    }
    
    func ambiguousDateFormatHour() -> String {
        return "dd/MM/yyyy - HH:MM"
    }
}
